
public class Libro {
	private long isbn;
	private int anoPub;
	private String titulo;
	private String autor;
	
	
	public Libro(){
	}
	
	public Libro(long isbn, int anoPub, String titulo, String autor) {
		this.isbn = isbn;
		this.anoPub = anoPub;
		this.titulo = titulo;
		this.autor = autor;
	}

	public long getISBN() {
		return isbn;
	}

	public void setISBN(long iSBN) {
		isbn = iSBN;
	}
	
	public int getAnoPub() {
		return anoPub;
	}
	
	public void setAnoPub(int anoPub) {
		this.anoPub = anoPub;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		Libro other = (Libro) obj;
		
		if (isbn != other.isbn)
			return false;
		
		if (anoPub != other.anoPub)
			return false;
		
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}

	public String toString() {
		return "ISBN: "+isbn+"\nAno de Publicacion: "+anoPub+ "\nTitulo: "+titulo+"\nAutor: "+autor+"\n";
	}

}
