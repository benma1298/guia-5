
public class Prestamo {
	private char tipomaterial;
	private long codmaterial;
	private String fechaprestamo;
	private String fechadevolucion;
	
	public Prestamo(char tipomaterial, long codmaterial, String fechaprestamo) {
		this.tipomaterial = tipomaterial;
		this.codmaterial = codmaterial;
		this.fechaprestamo = fechaprestamo;
	}	
	
	public Prestamo(char tipomaterial, long codmaterial, String fechaprestamo, String fechadevolucion) {
		this.tipomaterial = tipomaterial;
		this.codmaterial = codmaterial;
		this.fechaprestamo = fechaprestamo;
		this.fechadevolucion = fechadevolucion;
	}
	
	public char getTipomaterial() {
		return tipomaterial;
	}

	public void setTipomaterial(char tipomaterial) {
		this.tipomaterial = tipomaterial;
	}

	public long getCodmaterial() {
		return codmaterial;
	}

	public void setCodmaterial(long codmaterial) {
		this.codmaterial = codmaterial;
	}

	public String getFechaprestamo() {
		return fechaprestamo;
	}

	public void setFechaprestamo(String fechaprestamo) {
		this.fechaprestamo = fechaprestamo;
	}

	public String getFechadevolucion() {
		return fechadevolucion;
	}

	public void setFechadevolucion(String fechadevolucion) {
		this.fechadevolucion = fechadevolucion;
	}

	public String toString() {
		return "Prestamo:\n" + "Tipo de material: " + tipomaterial + "\nCodigo: " + codmaterial
				+ "\nFecha del prestamo: " + fechaprestamo + "\nFecha de devolucion: " + fechadevolucion;
	}

}
