
public class Revista {
	private long codrevista;
	private String nombre;
	private String tipo;
	
	public Revista() {
	}
	
	public Revista(long codrevista, String nombre, String tipo) {
		this.codrevista = codrevista;
		this.nombre = nombre;
		this.tipo = tipo;
	}
	
	public long getCodrevista() {
		return codrevista;
	}

	public void setCodrevista(long codrevista) {
		this.codrevista = codrevista;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Revista other = (Revista) obj;
		if (codrevista != other.codrevista)
			return false;
		
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Codigo de revista: "+codrevista+"\nNombre: "+ nombre +"\nTipo: "+ tipo;
	}

}
