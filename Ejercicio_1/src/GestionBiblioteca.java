
import java.util.ArrayList;

public class GestionBiblioteca {
	
	private GestionLibros gl = new GestionLibros();
	private GestionRevistas gr = new GestionRevistas();
	
	private ArrayList <Prestamo> prestamos = new ArrayList<Prestamo>();
	
	public void menu() {
		int opcion = 0;
		do {
			System.out.println("Base de Datos Biblioteca:");
			System.out.println("1. Gestion de Libros.");
			System.out.println("2. Gestion de Revistas.");
			System.out.println("3. Realizar Prestamo.");
			System.out.println("4. Devolver Prestamo.");
			System.out.println("5. Mostrar Prestamos.");
			System.out.println("0. Salir.");
			opcion = PedirDatos.leerEntero("Elija una opcion.");
			
			switch (opcion) {
			case 1:
				gestionLibros();
				break;
			case 2:
				gestionRevistas();
				break;
			case 3:
				prestar();
				break;
			case 4:
				devolver();
				break;
			case 5:
				prestado();
				break;
			case 0:
				System.out.println("El programa ha finalizado.");
				break;
			default:
				System.out.println("Debe introducir una opcion entre el 0 y 6.");
				break;
			}
		} while (opcion!=0);
	}
	
	private void gestionLibros() {
		gl.menu();
	}
	
	private void gestionRevistas() {
		gr.menu();
	}
	
	public void prestar() {
		int pos;
		
		char tipomaterial=PedirDatos.leerCaracter("Introduzca el tipo de material que desea. (L = Libro / R = Revista)");
		while (tipomaterial!='L'&&tipomaterial!='R') {
			tipomaterial=PedirDatos.leerCaracter("Valor incorrecto. Introduzca el tipo de material que desea. (L = Libro / R = Revista)");			
		}
		long codmaterial=0;
		switch (tipomaterial) {
		case 'L':
			codmaterial=PedirDatos.leerLong("Introduzca el ISBN del libro que desea sacar.");
			pos=gl.buscarLibro(codmaterial);
			if (pos==-1) {
				System.out.println("El libro con el ISBN "+codmaterial+" no existe.");
				return;
			}
			break;
		case 'R':
			codmaterial=PedirDatos.leerLong("Introduzca el codigo de la revista que desea sacar.");
			pos=gr.buscarRevista(codmaterial);
			if (pos==-1) {
				System.out.println("La revista con el codigo "+codmaterial+" no existe.");
				return;
			}
			break;
		}
		if (buscarPrestamo(tipomaterial, codmaterial)!=-1) {
			System.out.println("El material que desea pedir se encuentra prestado actualmente.");
			return;
		}
		String fechaprestamo=PedirDatos.leerCadena("Introduzca la fecha del préstamo (DD/MM/AAAA).");
		Prestamo p=new Prestamo(tipomaterial, codmaterial, fechaprestamo);
		this.prestamos.add(p);
		System.out.println("Prestamo realizado correctamente.");
	}
	
	
	public void devolver() {
		if (this.prestamos.isEmpty()) {
			System.out.println("No se pueden devolver prestamos porque todavia no se ha realizado ninguno.");
			return;
		}
		char tipomaterial=PedirDatos.leerCaracter("Introduzca el tipo de material que se presto.");
		long codmaterial=PedirDatos.leerLong("Introduzca el codigo del material que se presto.");
		int pos=buscarPrestamo(tipomaterial, codmaterial);
		if (pos==-1) {
			System.out.println("No existe ningun prestamo con los datos introducidos.");
			return;
		}
		String fechadevolucion=PedirDatos.leerCadena("Introduzca la fecha de devolucion (DD/MM/AAAA).");
		this.prestamos.get(pos).setFechadevolucion(fechadevolucion);
		System.out.println("Se ha devuelto el siguiente prestamo:\nCodigo del prestamo: "+(pos+1)+"\n"+this.prestamos.get(pos));
	}
	
	public void prestado() {
		for (int i = 0; i < this.prestamos.size(); i++) {
			System.out.println(this.prestamos.get(i));
			System.out.println("-------------------------");
		}
	}
	private int buscarPrestamo(char tipomaterial, long codmaterial) {
		for (int i = 0; i < this.prestamos.size(); i++) {
			if (tipomaterial==this.prestamos.get(i).getCodmaterial()&&codmaterial==this.prestamos.get(i).getCodmaterial()&&
					this.prestamos.get(i).getFechadevolucion()==null) {
				return i;
			}
		}
		return -1;
	}
}
