
import java.util.ArrayList;
	
public class GestionLibros {
	
private ArrayList <Libro> gestionlibros=new ArrayList<Libro>();
	
	public void menu() {
		int opcion=0;
		do {
			System.out.println("Gestion de Libros: ");
			System.out.println("1. Agregar libro.");
			System.out.println("2. Eliminar libro.");
			System.out.println("3. Mostrar libros.");
			System.out.println("0. Volver al menu principal.");
			opcion=PedirDatos.leerEntero("Elija una opcion.");
			
			switch (opcion) {
			case 1:
				agregarLibro();
				break;
			case 2:
				eliminarLibro();
				break;
			case 3:
				mostrarLibros();
				break;
			case 0:
				System.out.println("Volviendo al menu principal...");
				System.out.println();
				break;
			default:
				System.out.println("Debe introducir una opcion entre 0 y 3.");
				break;
			}			
		} while (opcion!=0);
	}

	private void agregarLibro() {
		
		long isbn=PedirDatos.leerLong("Introduzca el ISBN del libro que desea agregar.");
		if (buscarLibro(isbn)!=-1) {
			System.out.println("No se puede agregar el libro con el ISBN: "+isbn+" porque ya existe.");
			return;
		}
		int anoPub=PedirDatos.leerEntero("Introduzca el ano de Publicacion.");
		String titulo=PedirDatos.leerCadena("Introduzca el titulo.");
		String autor=PedirDatos.leerCadena("Introduzca el autor.");
		Libro l=new Libro(isbn, anoPub, titulo, autor);
		this.gestionlibros.add(l);
		System.out.println("El libro con el ISBN: "+isbn+" se ha creado correctamente.");
	}

	private void eliminarLibro() {
		if (this.gestionlibros.isEmpty()) {
			System.out.println("No puede eliminar libros porque no existe ninguno.");
			return;
		}
		long isbn=PedirDatos.leerLong("Introduzca el ISBN del libro que desea eliminar.");
		int pos=buscarLibro(isbn);
		if (pos==-1) {
			System.out.println("No se puede eliminar el libro con el ISBN "+pos+" porque no existe.");
			return;
		}
		this.gestionlibros.remove(pos);
		System.out.println("El libro con el ISBN "+isbn+" ha sido eliminado correctamente.");
	}

	private void mostrarLibros() {
		for (int i = 0; i < this.gestionlibros.size(); i++) {
			System.out.println(this.gestionlibros.get(i));
			System.out.println("-------------------------");
		}
	}
	
	public int buscarLibro(long isbn) {
		for (int i = 0; i < this.gestionlibros.size(); i++) {
			if (this.gestionlibros.get(i).getISBN()==isbn) {
				return i;
			}
		}
		return -1;
	}
}
