
import java.util.ArrayList;

public class GestionRevistas {
private ArrayList <Revista> gestionrevistas=new ArrayList <Revista>();
	
	public void menu() {
		int opcion=0;
		do {
			System.out.println("Gestion de Revistas: ");
			System.out.println("1. Agregar revista.");
			System.out.println("2. Eliminar revista.");
			System.out.println("3. Mostrar revistas.");
			System.out.println("0. Volver al menu principal.");
			opcion=PedirDatos.leerEntero("Elija una opcion.");
			
			switch (opcion) {
			case 1:
				agregarRevista();
				break;
			case 2:
				eliminarRevista();
				break;
			case 3:
				mostrarRevistas();
				break;
			case 0:
				System.out.println("Volviendo al menu principal...");
				System.out.println();
				break;
			default:
				System.out.println("Debe introducir una opcion entre 0 y 3.");
				break;
			}
		} while (opcion!=0);
	}

	private void agregarRevista() {

		long codrevista=PedirDatos.leerLong("Introduzca el codigo de la revista que desea agregar.");
		if (buscarRevista(codrevista)!=-1) {
			System.out.println("No puede agregar la revista con el codigo "+codrevista+" porque ya existe.");
			return;
		}
		String nombre=PedirDatos.leerCadena("Introduzca el nombre de la revista.");
		String tipo=PedirDatos.leerCadena("Introduzca el tipo de revista. ");
		Revista r=new Revista(codrevista, nombre, tipo);
		this.gestionrevistas.add(r);
		System.out.println("La revista con el codigo "+codrevista+" se ha agregado correctamente.");
	}

	private void eliminarRevista() {
		if (this.gestionrevistas.isEmpty()) {
			System.out.println("No puede eliminar revistas porque no existe ninguna.");
			return;
		}
		long codrevista=PedirDatos.leerLong("Introduzca el codigo de la revista que desea eliminar.");
		int pos=buscarRevista(codrevista);
		if (pos==-1) {
			System.out.println("No se puede eliminar la revista con el codigo "+codrevista+" porque no existe.");
			return;
		}
		this.gestionrevistas.remove(pos);
		System.out.println("La revista con el codigo "+codrevista+" ha sido eliminada correctamente.");
	}

	private void mostrarRevistas() {
		for (int i = 0; i < this.gestionrevistas.size(); i++) {
			System.out.println(this.gestionrevistas.get(i));
			System.out.println("-------------------------");
		}
	}
	
	public int buscarRevista(long codrevista) {
		for (int i = 0; i < this.gestionrevistas.size(); i++) {
			if (this.gestionrevistas.get(i).getCodrevista()==codrevista) {
				return i;
			}
		}
		return -1;
	}

}
