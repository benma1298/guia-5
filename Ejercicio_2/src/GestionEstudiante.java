import java.util.ArrayList;

public class GestionEstudiante {

	private ArrayList <Estudiante> gestionEstudiante = new ArrayList<Estudiante>();
	
	public void menu() {
		int opcion = 0;
		do {
			System.out.println("Gestion de Estudiantes.");
			System.out.println("1. Agregar Estudiante.");
			System.out.println("2. Mostrar Estudiantes.");
			System.out.println("0. Volver al Menu Principal");
			opcion = PedirDatos.leerEntero("Elija una opcion");
			switch(opcion) {
			case 1:
				agregarEstudiante();
				break;
			case 2:
				mostrarEstudiante();
				break;
			case 0:
				System.out.println("Volviendo...");
				System.out.println();
				break;
			default:
				System.out.println("Error, favor introducir opcion entre 0 y 2");
				break;
			}
		}while(opcion!=0);
	}
	
	private void agregarEstudiante() {
		long numId=PedirDatos.leerLong("Introduzca el Numero de Identificacion del personal administrativo que desea agregar.");
		if(buscarEstudiante(numId)!=-1) {
			System.out.println("Error," + numId + " ya existe.");
			return;
		}
		String nombre = PedirDatos.leerCadena("Introduzca el nombre.");
		String apellidos = PedirDatos.leerCadena("Introduzca los apellidos.");
		String curso = PedirDatos.leerCadena("Introduzca el curso al que pertenece.");
		Estudiante a = new Estudiante(numId, nombre, apellidos, curso);
		this.gestionEstudiante.add(a);
		System.out.println("Se agrego correctamente el estudiante con numero de identificacion: " + numId + "\n");
	}
	
	public void mostrarEstudiante() {
		for(int i = 0; i < this.gestionEstudiante.size(); i++) {
			System.out.println(this.gestionEstudiante.get(i));
			System.out.println(".....................");
		}
	}
	
	public int buscarEstudiante(long numId) {
		for (int i = 0; i < this.gestionEstudiante.size(); i++) {
			if (this.gestionEstudiante.get(i).getNumId()==numId) {
				return i;
			}
		}
		return -1;
	}

}
