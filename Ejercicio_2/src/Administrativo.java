
public class Administrativo extends Persona{
	
	private int anoIn;
	private int numAnexo;
	private String seccion;
	
	public Administrativo() {
	}
	
	public Administrativo(long numId, String nombre, String apellidos, int anoIn, int numAnexo, String seccion) {
		super(numId, nombre, apellidos);
		this.anoIn = anoIn;
		this.numAnexo = numAnexo;
		this.seccion = seccion;
	}
	
	public int getAnoIn() {
		return anoIn;
	}
	public void setAnoIn(int anoIn) {
		this.anoIn = anoIn;
	}
	public int getNumAnexo() {
		return numAnexo;
	}
	public void setNumAnexo(int numAnexo) {
		this.numAnexo = numAnexo;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nAno de Ingreso: " + anoIn + "\nNumero de Anexo: " + numAnexo + "\nSeccion: " + seccion + "\n";
	}

}
