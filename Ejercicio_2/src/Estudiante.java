
public class Estudiante extends Persona {
	
	private String curso;
	
	public Estudiante() {
	}
	
	public Estudiante(long numId, String nombre, String apellidos, String curso) {
		super(numId, nombre, apellidos);
		this.curso = curso;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nCurso Matriculado: " + curso + "\n";
	}

}
