
import java.util.ArrayList;

public class GestionProfesor {
	
	private ArrayList <Profesor> gestionProfesor = new ArrayList<Profesor>();
	
	public void menu() {
		int opcion = 0;
		do {
			System.out.println("Gestion de Profesores.");
			System.out.println("1. Agregar Profesor.");
			System.out.println("2. Mostrar Profesores.");
			System.out.println("0. Volver al Menu Principal");
			opcion = PedirDatos.leerEntero("Elija una opcion");
			switch(opcion) {
			case 1:
				agregarProfesor();
				break;
			case 2:
				mostrarProfesores();
				break;
			case 0:
				System.out.println("Volviendo...");
				System.out.println();
				break;
			default:
				System.out.println("Error, favor introducir opcion entre 0 y 2");
				break;
			}
		}while(opcion!=0);
	}
	
	private void agregarProfesor() {
		long numId=PedirDatos.leerLong("Introduzca el Numero de Identificacion del profesor que desea agregar.");
		if(buscarProfesor(numId)!=-1) {
			System.out.println("Error," + numId + " ya existe.");
			return;
		}
		String nombre = PedirDatos.leerCadena("Introduzca el nombre.");
		String apellidos = PedirDatos.leerCadena("Introduzca los apellidos.");
		int anoIn = PedirDatos.leerEntero("Introduzca el ano de incorporacion.");
		int numAnexo = PedirDatos.leerEntero("Introduzca el numero de anexo asignado.");
		String departamento = PedirDatos.leerCadena("Introduzca el departamento al que pertenece.");
		Profesor p = new Profesor(numId, nombre, apellidos, anoIn, numAnexo, departamento);
		this.gestionProfesor.add(p);
		System.out.println("Se agrego correctamente al profesor con numero de identificacion: " + numId + "\n");
	}
	
	public void mostrarProfesores() {
		for(int i = 0; i < this.gestionProfesor.size(); i++) {
			System.out.println(this.gestionProfesor.get(i));
			System.out.println(".....................");
		}
	}
	
	public int buscarProfesor(long numId) {
		for (int i = 0; i < this.gestionProfesor.size(); i++) {
			if (this.gestionProfesor.get(i).getNumId()==numId) {
				return i;
			}
		}
		return -1;
	}
}
