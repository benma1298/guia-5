
public class Profesor extends Persona {
	
	private int anoIn;
	private int numAnexo;
	private String departamento;
	
	public Profesor() {
	}

	public Profesor(long numId, String nombre, String apellidos, int anoIn, int numAnexo, String departamento) {
		super(numId, nombre, apellidos);
		this.anoIn = anoIn;
		this.numAnexo = numAnexo;
		this.departamento = departamento;
	}
	
	public int getAnoIn() {
		return anoIn;
	}
	public void setAnoIn(int anoIn) {
		this.anoIn = anoIn;
	}
	public int getNumAnexo() {
		return numAnexo;
	}
	public void setNumAnexo(int numAnexo) {
		this.numAnexo = numAnexo;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nAno de Ingreso: " + anoIn + "\nNumero de Anexo: " + numAnexo + "\nDepartamento: " + departamento + "\n";
	}
}
