
import java.util.ArrayList;

public class GestionAdministrativo {

	private ArrayList <Administrativo> gestionAdministrativo = new ArrayList<Administrativo>();
	
	public void menu() {
		int opcion = 0;
		do {
			System.out.println("Gestion de Personal Administrativo.");
			System.out.println("1. Agregar Personal Administrativo.");
			System.out.println("2. Mostrar Personal Administrativo.");
			System.out.println("0. Volver al Menu Principal");
			opcion = PedirDatos.leerEntero("Elija una opcion");
			switch(opcion) {
			case 1:
				agregarAdministrativo();
				break;
			case 2:
				mostrarAdministrativo();
				break;
			case 0:
				System.out.println("Volviendo...");
				System.out.println();
				break;
			default:
				System.out.println("Error, favor introducir opcion entre 0 y 2");
				break;
			}
		}while(opcion!=0);
	}
	
	private void agregarAdministrativo() {
		long numId=PedirDatos.leerLong("Introduzca el Numero de Identificacion del personal administrativo que desea agregar.");
		if(buscarAdministrativo(numId)!=-1) {
			System.out.println("Error," + numId + " ya existe.");
			return;
		}
		String nombre = PedirDatos.leerCadena("Introduzca el nombre.");
		String apellidos = PedirDatos.leerCadena("Introduzca los apellidos.");
		int anoIn = PedirDatos.leerEntero("Introduzca el ano de incorporacion.");
		int numAnexo = PedirDatos.leerEntero("Introduzca el numero de anexo asignado.");
		String seccion = PedirDatos.leerCadena("Introduzca la seccion a la que pertenece.");
		Administrativo a = new Administrativo(numId, nombre, apellidos, anoIn, numAnexo, seccion);
		this.gestionAdministrativo.add(a);
		System.out.println("Se agrego correctamente al personal administrativo con numero de identificacion: " + numId + "\n");
	}
	
	public void mostrarAdministrativo() {
		for(int i = 0; i < this.gestionAdministrativo.size(); i++) {
			System.out.println(this.gestionAdministrativo.get(i));
			System.out.println(".....................");
		}
	}
	
	public int buscarAdministrativo(long numId) {
		for (int i = 0; i < this.gestionAdministrativo.size(); i++) {
			if (this.gestionAdministrativo.get(i).getNumId()==numId) {
				return i;
			}
		}
		return -1;
	}
}
