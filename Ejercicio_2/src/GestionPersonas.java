
public class GestionPersonas {
	
	private GestionProfesor gp = new GestionProfesor();
	private GestionAdministrativo ga = new GestionAdministrativo();
	private GestionEstudiante ge = new GestionEstudiante();
	
	public void menu() {
		int opcion = 0;
		do {
			System.out.println("Integrantes de la Universidad");
			System.out.println("1. Gestion de Profesores.");
			System.out.println("2. Gestion de Administrativos.");
			System.out.println("3. Gestion de Estudiantes.");
			System.out.println("4. Mostrar Integrantes.");
			System.out.println("0. Salir.");
			opcion = PedirDatos.leerEntero("Elija una opcion.");
			
			switch(opcion) {
			case 1:
				gestionProfesor();
				break;
			case 2:
				gestionAdministrativo();
				break;
			case 3:
				gestionEstudiante();
				break;
			case 4:
				mostrarIntegrantes();
				break;
			case 0:
				System.out.println("El sistema de gestion ha concluido");
				break;
			default:
				System.out.println("Debe introducir una opcion entre el 0 y 3.");
				break;
			}
		}while(opcion!=0);
	}
	
	private void gestionProfesor() {
		gp.menu();
	}
	private void gestionAdministrativo() {
		ga.menu();
	}
	private void gestionEstudiante() {
		ge.menu();
	}
	
	private void mostrarIntegrantes() {
		ga.mostrarAdministrativo();
		gp.mostrarProfesores();
		ge.mostrarEstudiante();
	}
}
