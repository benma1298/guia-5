
public class Persona {
	
	private long numId;
	private String nombre;
	private String apellidos;
	
	public Persona() {
	}
	
	public Persona(long numId, String nombre, String apellidos) {
		this.numId = numId;
		this.nombre = nombre;
		this.apellidos = apellidos;
	}
	
	public long getNumId() {
		return numId;
	}

	public void setNumId(long numId) {
		this.numId = numId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	@Override
	public String toString() {
		return "Nombre: " + nombre + " " + apellidos +"\nNumero de Identificacion: "+ numId + "\n";
	}
	

}
